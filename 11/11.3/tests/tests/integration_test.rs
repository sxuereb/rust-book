use tests;
mod common;

#[test]
fn id_adds_two() {
    common::setup();
    assert_eq!(4, tests::add_two(2));
}
