struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn main() {
    println!("Hello, world!");
    let mut user1 = User {
        username: String::from("user"),
        email: String::from("some@example.com"),
        active: true,
        sign_in_count: 1,
    };

    user1.email = String::from("mut@example.com");

    let user2 = build_user(String::from("other@example.com"), String::from("user2"));

    let user3 = User {
        email: String::from("another@example.com"),
        username: String::from("yetanother"),
        ..user2
    };

    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    }
}
