#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

impl Rectangle {
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn main() {
    // Define a rectengale with just variables.
    // let width1 = 30;
    // let height1 = 50;

    // Define a rectenagle as a tuple.
    // let rect1 = (30, 50);

    // Define a rectenagle as a struct.
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    let rect2 = Rectangle {
        width: 10,
        height: 40,
    };

    let rect3 = Rectangle {
        width: 60,
        height: 45,
    };

    let sq = Rectangle::square(3);

    // Using functin.
    // println!(
    //     "The area of the rectenagle is {} square pixels.",
    //     area(&rect1)
    // );

    println!("Can rect1 hold rect2? {}", rect1.can_hold(&rect2));
    println!("Can rect1 hold rect3? {}", rect1.can_hold(&rect3));
    println!(
        "The area of the rectangle is {} squre pixels.",
        rect1.area()
    );

    println!("square is {:#?}", sq);
}

// Calculate using parameters.
// fn area(width: u32, height: u32) -> u32 {
//     width * height
// }

// Calculate usig tuples
// fn area(dimensions: (u32, u32)) -> u32
//     dimensions.0 * dimensions.1
// }

// Calculate using struct.
// fn area(rectangle: &Rectangle) -> u32 {
//     rectangle.width * rectangle.height
// }
