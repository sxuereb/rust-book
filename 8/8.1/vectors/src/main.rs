fn main() {
    let v = vec![1, 2, 3, 4, 5];

    // Might panic if out of range.
    let thrid: &i32 = &v[2];
    println!("The thrid element is {}", thrid);

    // Handle err if index of out range.
    match v.get(2) {
        Some(thrid) => println!("The thrid element is {}", thrid),
        None => println!("There is no thrid element."),
    }

    // Iterate over values.
    for i in &v {
        println!("{}", i);
    }

    // Iterate over values mutating them.
    let mut v = vec![100, 32, 57];
    for i in &mut v {
        *i += 50;
        println!("{}", i);
    }

    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
}
