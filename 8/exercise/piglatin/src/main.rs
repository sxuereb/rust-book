fn main() {
    let apple = piglatin("apple");
    println!("apple in piglatin is: {}", apple);

    let first = piglatin("first");
    println!("first in piglatin is: {}", first);
}

// first irst-fay
// apple apple-hay
fn piglatin(s: &str) -> String {
    // Create list of vowels
    let vowels: [char; 5] = ['a', 'e', 'i', 'o', 'u'];

    let first_char = s.chars().next();
    match first_char {
        Some(c) => {
            if vowels.contains(&c) {
                return format!("{}-hay", &s[c.len_utf8()..]);
            }

            return format!("{}-{}ay", &s[c.len_utf8()..], c);
        }
        None => String::new(),
    }
}
