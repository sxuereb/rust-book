use std::collections::HashMap;

fn main() {
    let mut v: Vec<i32> = vec![3, 4, 1, 2, 5, 4];

    let avg = average(&v);
    println!("The everage is: {}", avg);

    let median = median(v.as_mut_slice());
    println!("The median is {}", median);

    let mode = mode(&v);
    println!("The mode is {}", mode)
}

fn average(v: &[i32]) -> i32 {
    let mut total = 0;
    for i in v {
        total += i
    }

    let len: i32 = v.len() as i32;

    total / len
}

fn median(v: &mut [i32]) -> i32 {
    v.sort();

    println!("{:?}", v);

    let median_index = v.len() / 2;

    v[median_index]
}

fn mode(v: &[i32]) -> i32 {
    // Go through the vector to find the frequency of each value.
    let mut frequency: HashMap<i32, i32> = HashMap::new();
    for i in v {
        *frequency.entry(*i).or_insert(0) += 1;
    }

    // Find the highest frequency.
    let mut max: i32 = 0;
    let mut mode: i32 = 0;
    for (&num, &freq) in frequency.iter() {
        if freq > max {
            max = freq;
            mode = num;
        }
    }

    mode
}
