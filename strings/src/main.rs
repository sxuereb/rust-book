fn main() {
    // Copy vs ownership
    let s = String::from("hello");
    takes_ownership(s);
    let x = 5;
    makes_copy(x);

    // Muteable reference
    let mut s = String::from("hello");
    change(&mut s);
    println!("s is: {}", s);
}

fn takes_ownership(some_string: String) {
    println!("{}", some_string);
}

fn makes_copy(some_integer: i32) {
    println!("{}", some_integer);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
