fn main() {
    let x = 5;

    let x = x + 1;

    let x = x * 2;

    println!("The value of x is: {}", x);

    let spaces = "    ";
    let spaces = spaces.len();

    println!("This much: {}", spaces);

    let guess: u32 = "42".parse().expect("Not a number!");

    println!("guess: {}", guess)
}
